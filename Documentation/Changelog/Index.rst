.. include:: /Includes.rst.txt

.. _changelog:

Changelog
=========

(Currently the rendering of this changelog is flawed; a more readable version can be found in the `repository`_)

.. _repository: https://gitlab.com/jigal/t3adminer/-/blob/master/CHANGELOG.md

.. include:: ./CHANGELOG.md
