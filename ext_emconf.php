<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'Adminer',
    'description' => 'Database administration tool \'Adminer\'',
    'category' => 'module',
    'author' => 'Jigal van Hemert',
    'author_email' => 'jigal.van.hemert@typo3.org',
    'author_company' => '',
    'state' => 'stable',
    'version' => '12.0.0',
    'clearCacheOnLoad' => false,
    'autoload' => [
        'psr-4' => [
            'jigal\t3adminer\\' => 'Classes'
        ],
    ],
    'constraints' => [
        'depends' => [
            'php' => '7.4.0-8.2.99',
            'typo3' => '11.5.0-12.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
